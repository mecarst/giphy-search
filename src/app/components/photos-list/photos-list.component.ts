import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { environment } from '../../../environments/environment';

import { SearchService, Giphy, Photos } from 'src/app/services/search.service';
import { PagerService, Pager } from 'src/app/services/pager.service';

@Component({
  selector: 'app-photos-list',
  templateUrl: './photos-list.component.html',
  styleUrls: ['./photos-list.component.scss']
})
export class PhotosListComponent implements OnInit {
  requestKey: string = environment.apiKey;
  photos$: Subject<any>; 
  photos: Photos[];
  pager: Pager;

  totalPhotosCount: number = 0;
  paginationOffset: number = 0;
  pagedItems: Photos[];
  searchTerm: String;

  constructor(
    private searchService: SearchService,
    private pagerService: PagerService
  ) {
  }

  ngOnInit() {
    this.photos$ = this.searchService.searchResults;
    this.photos$.subscribe((res: Giphy) => {
      if (!res.data) return;
      const { data, queryString } = res;

      this.searchTerm = queryString;
      this.totalPhotosCount = data.length;
      this.photos = data;
      this.setPage(1);
    })
  }

  setPage(pageNumber: number) {
    if (pageNumber < 1) return;

    this.pager = this.pagerService.getPager(pageNumber, this.totalPhotosCount);
    this.pagedItems = this.photos.slice(this.pager.startIndex, this.pager.endIndex + 1);

    if ((pageNumber >= this.pager.totalPages)) {
      this.pager.totalPages += this.pager.totalPages;
      this.paginationOffset = this.pager.totalItems;
      this.pager.startPage++;
      this.pager.currentPage++;
      // request more
      this.searchService.search(this.searchTerm, this.paginationOffset);
    }
  }
}
