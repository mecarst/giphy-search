import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { SearchService } from 'src/app/services/search.service';
import { validateSearchQuery } from 'src/app/utils/utils';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  photos = new FormControl('', [Validators.required, validateSearchQuery]);
  errorMsg: string = '';

  constructor(
    private searchService: SearchService
  ) {}

  ngOnInit(): void {}

  searchPhotos(): void {
    if (!this.photos.valid || this.photos.value === '') {
      if (this.photos.errors && this.photos.errors.invalidQuery) {
        this.errorMsg = 'no bad words';
      }
      return;
    };
    this.searchService.search(this.photos.value);
  }

  onKeydown($event: KeyboardEvent) {
    if ($event?.key === "Enter") {
      this.searchPhotos();
    }
  }
}
