import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { SearchService } from '../../services/search.service';
import { PhotosListComponent } from 'src/app/components/photos-list/photos-list.component';

@NgModule({
  declarations: [
    PhotosListComponent
  ],
  providers: [SearchService],
  imports: [
    CommonModule,
    NgxPaginationModule
  ],
  exports: [
    PhotosListComponent,
  ]
})
export class SearchModule { }
