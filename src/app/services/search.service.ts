import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';

export interface PhotoInfo {
  height: String;
  size: String;
  url: String;
  width: String;
}

export interface Photo {
  downsized_large: {
    [key: string]: PhotoInfo
  };
}

export interface Photos {
  type: String;
  id: String;
  url: String;
  title: String;
  import_datetime: Date;
  images: {
    [key: string]: Photo
  }
}

export interface Pagination {
  total_count: number;
  count: number;
  offset: number;
}

export interface Giphy {
  data: Array<Photos>;
  pagination: Pagination;
  meta: Object;
  queryString: String;
}

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  apiKey: String = environment.apiKey;
  configUrl = 'api.giphy.com/v1/gifs/search?api_key=';
  searchResults = new Subject<Giphy>();
  response = this.searchResults.asObservable();

  constructor(
    private http: HttpClient
  ) {}

  search(query: String, offset?: Number): void {
    let url = `https://${this.configUrl}${this.apiKey}&q=${query}&limit=100`;

    if (offset) {
      url += `&offset=${offset}`;
    }

    this.http.get(url).subscribe((data: Giphy) => {
      data.queryString = query;
      this.searchResults.next(data);
    });
  }
}
