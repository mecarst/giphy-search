import { TestBed } from '@angular/core/testing';

import { PagerService, Pager } from './pager.service';

describe('PagerService', () => {
  let service: PagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PagerService]
    });
    service = TestBed.inject(PagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getPager should call getPager', () => {
    spyOn(service, 'getPager');
    service.getPager(1, 20);
    expect(service.getPager).toHaveBeenCalled();
  });

  it('getPager should return an object data', () => {
    const data = service.getPager(1, 20);
    const res: Pager = {
      totalItems: 20,
      currentPage: 1,
      pageSize: 10,
      totalPages: 2,
      startPage: 1,
      endPage: 2,
      startIndex: 0,
      endIndex: 9,
      pages: [1,2],
      offset: 0
    }
    expect(data).toEqual(res);
  });
});
