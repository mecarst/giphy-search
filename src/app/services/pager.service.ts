import { Injectable } from '@angular/core';
import { range } from 'src/app/utils/utils';

export interface Pager {
  totalItems: number;
  currentPage: number;
  pageSize: number;
  totalPages: number;
  startPage: number;
  endPage: number;
  startIndex: number;
  endIndex: number;
  pages: Array<number>;
  offset: number;
}

@Injectable({
  providedIn: 'root'
})
export class PagerService {
  getPager (
    currentPage: number = 1,
    totalItems: number,
    pageSize: number = 9
  ): Pager {
    
    let totalPages = Math.ceil(totalItems / pageSize);
    let startPage: number, endPage: number, offset: number;
    
    if (totalPages <= 9) {
      startPage = 1;
      endPage = totalPages;
      offset = 0;
    } else {
      if (currentPage >= totalPages) {
        startPage = totalPages - pageSize;
        endPage = totalPages;
        offset = 100;
      }else {
        startPage = currentPage;
        endPage = currentPage + 1;
        offset = 0;
      }
    }

    if (currentPage < 1) currentPage = 1;

    let pages = range(currentPage, totalPages);
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages,
      offset: offset
    }
  }

  next(currentPage: number = 1) {
    return currentPage + 1;
  }

  prev(currentPage: number = 1) {
    return currentPage - 1;
  }

}
